<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class Blogseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Blog::class,1000)->create();
    }
    }

